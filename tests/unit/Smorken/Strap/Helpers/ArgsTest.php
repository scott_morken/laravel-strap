<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/17/14
 * Time: 11:55 AM
 */

namespace unit\Smorken\Strap\Helpers;

use Smorken\Strap\Helpers\Args;

class ArgsTest extends \PHPUnit_Framework_TestCase {

    public function testGetArgs()
    {
        $args = array('myname');
        $expected = array('myname', null, array());
        $this->assertEquals($expected, array_values(Args::getArgs('text', $args)));
    }

    public function testGetArgsDoesntOverwriteSetValue()
    {
        $args = array('myname', 'myvalue');
        $expected = array('name' => 'myname', 'value' => 'myvalue', 'options' => array());
        $this->assertEquals($expected, Args::getArgs('text', $args));
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testGetArgsMissingRequired()
    {
        $args = array('name', 1);
        Args::getArgs('selectRange', $args);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testGetArgsInvalidType()
    {
        $args = array('namename');
        Args::getArgs('foo', $args);
    }
} 