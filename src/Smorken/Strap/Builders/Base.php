<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/17/14
 * Time: 8:59 AM
 */

namespace Smorken\Strap\Builders;

use Smorken\Strap\Parts\Label;
use Smorken\Strap\Parts\Wrapper;
use Smorken\Strap\Parts\Input;
use Smorken\Strap\Strap;
use Smorken\Strap\Builders\Modifiers\ModHandler;

class Base {

    /**
     * @var \Smorken\Strap\Builders\Modifiers\ModHandler
     */
    protected $modifier;

    protected $label;

    protected $field;

    protected $id;

    protected $field_type;

    protected $help;

    protected $error;

    protected $formBuilder;

    /**
     * @var \Smorken\Strap\Strap
     */
    protected $strap;

    protected $types = array(
        'default',
    );

    public function init(Strap $strap, $overrides = array())
    {
        $this->setStrap($strap);
        $this->setFormBuilder($this->getStrap()->getFormBuilder());
        $this->modifier = new ModHandler($this->getStrap(), $this->getStrap()->getBuilderName(), $overrides);
    }

    /**
     * @return ModHandler
     */
    public function mod()
    {
        if (!$this->modifier && $this->getStrap()) {
            $builder = snake_case(class_basename($this));
            $this->modifier = new ModHandler($this->getStrap(), $builder);
        }
        return $this->modifier;
    }

    public function build($fields = array())
    {
        if ($fields) {
            $w = new Wrapper('');
            $newfields = array();
            foreach($fields as $field) {
                if ($field instanceof Strap) {
                    $field->wrap(false);
                    $field->isGrouped(true);
                    $field->builder(class_basename($this));
                    $newfields[] = $field->toWrapper();
                }
                else {
                    $newfields[] = $field;
                }
            }
            $w->setValue($newfields);
            $this->field = $w;
            $this->field_type = 'multi';
        }
        else {
            $this->buildField();
        }
        $this->buildLabel();
        $this->buildHelp();
        $this->buildError();
        $wrapopts = $this->getWrapOptions();
        $items = $this->wrapIndividualItems($wrapopts);
        if ($this->getStrap()->getWrap()) {
            $grouped = $this->groupItems($items);
            $wrapped = $this->wrapGroupedItems($grouped);
        }
        else {
            $wrapped = $items;
        }
        $outer = $this->createOuterWrapper($wrapped);
        return $outer;
    }

    public function createOuterWrapper($items)
    {
        $wrapper = new Wrapper('');
        $wrapper->setValue($items);
        return $this->wrapper('wrap_mod', 'outer', $wrapper);
    }

    public function wrapGroupedItems($grouped)
    {
        $strap_type = $this->getStrap()->getType();
        $wrapped = array();
        foreach($grouped as $col => $items) {
            $wrapper = new Wrapper('');
            $wrapper->setValue($items);
            $paths = array(
                implode('.', array('wrap_mod', 'group', $col)),
                implode('.', array('wrap_mod', 'group', $strap_type)),
                implode('.', array('wrap_mod', 'group', $strap_type, $col)),
                implode('.', array('wrap_mod', 'group', $col, $this->field_type)),
                implode('.', array('wrap_mod', 'group', $strap_type, $col, $this->field_type))
            );

            $wrapped[$col] = $this->wrapper('wrap_mod', 'group', $wrapper, $paths);
        }
        return $wrapped;
    }

    public function groupItems($items)
    {
        $groups = array();

        if ($this->getStrap()->getType() == 'horizontal') {
            if (!$this->getStrap()->getInside()) {
                $groups['col-1'] = array('label');
                $groups['col-2'] = array(
                    'field',
                    'help',
                    'error',
                );
            }
            else {
                $groups['col-inside'] = array(
                    'label',
                    'help',
                    'error',
                );
            }
        }
        else {
            if (!$this->getStrap()->getInside()) {
                $groups['col'] = array('label', 'field', 'help', 'error');
            }
            else {
                $groups['col'] = array('label', 'help', 'error');
            }
        }
        $grouped = array();
        foreach($groups as $col => $pieces) {
            $tempItems = array();
            foreach($pieces as $piece) {
                $tempItems[$piece] = $items[$piece];
            }
            $grouped[$col] = $tempItems;
        }
        return $grouped;
    }

    public function wrapIndividualItems($wraps = array())
    {
        $items = array();
        foreach($wraps as $item => $addPaths)
        {
            $this->doWrap($item, $addPaths);
            $items[$item] = $this->$item;
        }
        return $items;
    }

    public function getWrapOptions()
    {
        $base = array(
            'label' => array(),
            'field' => array(),
            'help' => array(),
            'error' => array(),
        );
        if ($this->getStrap()->getInside()) {
            unset($base['field']);
            $base['label'][] = 'wrap_mod.label.inside';
            $base['label'][] = 'wrap_mod.label.inside.' . $this->field_type;
        }
        return $base;
    }

    protected function checkWrap($what)
    {
        if ($this->$what) {
            return true;
        }
        return false;
    }

    public function doWrap($what, $additionalPaths = array())
    {
        if ($this->checkWrap($what)) {
            $this->$what = $this->wrapper('wrap_mod', $what, $this->$what, $additionalPaths);
        }
    }

    public function buildField()
    {
        $data = $this->getStrap()->getField();
        $for = 'field';
        $this->field_type = $data['type'];
        if (array_key_exists('value', $data['args'])) {
            if ($this->getStrap()->getValue()) {
                $data['args']['value'] = $this->getStrap()->getValue();
            }
        }
        if (array_key_exists('selected', $data['args'])) {
            if ($this->getStrap()->getValue()) {
                $data['args']['selected'] = $this->getStrap()->getValue();
            }
        }
        $w = new Input($this->formBuilder(), $data['type'], $data['args']);
        $this->id = $w->getId();
        $this->field = $this->wrapper('attr_mod', $for, $w);
    }

    public function buildLabel()
    {
        $for = 'label';
        $data = $this->getStrap()->getLabel();
        $name = $this->id ?: $this->getStrap()->getName();
        $value = $data['label'];
        if ($value === false) {
            return;
        }
        $values = array();
        $values[] = $this->formBuilder()->_formatLabel($name, $value);
        $additionalWraps = array();
        if ($this->getStrap()->getInside()) {
            if ($this->getStrap()->isRight()) {
                array_unshift($values, $this->getField());
            }
            else {
                $values[] = $this->getField();
            }
            $additionalWraps[] = 'attr_mod.inside.label';
            $additionalWraps[] = 'attr_mod.inside.label.' . $this->field_type;
        }


        $attributes = $data['options'];
        $label = new Label($name, $values, $attributes);

        $this->label = $this->wrapper('attr_mod', $for, $label, $additionalWraps);
    }

    public function buildHelp()
    {
        $for = 'help';
        $data = $this->getStrap()->getHelp();
        $attributes = $data['options'];
        $default_tag = $attributes ? 'span' : '';
        $w = new Wrapper($default_tag, $attributes);
        $w->setValue($data['text']);
        $this->help = $this->wrapper('attr_mod', $for, $w);
    }

    public function buildError()
    {
        $for = 'error';
        if (!$this->getStrap()->getError()['text']) {
            return;
        }
        $data = $this->getStrap()->getError();
        $attributes = $data['options'];
        $default_tag = $attributes ? 'span' : '';
        $w = new Wrapper($default_tag, $attributes);
        $w->setValue($data['text']);
        $this->error = $this->wrapper('attr_mod', $for, $w);
    }

    public function wrapper($mod_type = 'attr_mod', $for, $wrapper, $additionalPaths = array())
    {
        $strap_type = $this->getStrap()->getType();
        $paths = array(
            implode('.', array($mod_type, $for)),
            implode('.', array($mod_type, $strap_type, $for)),
            implode('.', array($mod_type, $for, $this->field_type)),
            implode('.', array($mod_type, $strap_type, $for, $this->field_type))
        );
        $paths = array_merge($paths, $additionalPaths);
        foreach($paths as $path) {
            $wrapper = $this->handleWrapperCall($path, $wrapper);
        }
        return $wrapper;
    }

    protected function handleWrapperCall($path, $wrapper)
    {
        return $this->mod()->wrapper()->mod($path, 0, $wrapper);
    }

    public function getField()
    {
        return $this->field;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getHelp()
    {
        return $this->help;
    }

    public function getError()
    {
        return $this->error;
    }



    /**
     * @param Strap $strap
     */
    public function setStrap(Strap $strap)
    {
        $this->strap = $strap;
        if ($this->mod()) {
            $this->mod()->setStrap($strap);
        }
    }

    /**
     * @throws BuilderException
     * @return Strap
     */
    public function getStrap()
    {
        if (!$this->strap) {
            throw new BuilderException("The strap object has not been instantiated on the builder.");
        }
        return $this->strap;
    }

    public function setFormBuilder($formBuilder)
    {
        $this->formBuilder = $formBuilder;
    }

    /**
     * @return \Smorken\Strap\FormBuilder
     */
    public function formBuilder()
    {
        return $this->formBuilder;
    }

}