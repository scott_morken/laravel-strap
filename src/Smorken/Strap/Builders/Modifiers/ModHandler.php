<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/18/14
 * Time: 1:29 PM
 */

namespace Smorken\Strap\Builders\Modifiers;


class ModHandler {

    protected $types = array(
        'base' => array(
            'wrapper' => 'Smorken\Strap\Builders\Modifiers\Wrapper\Base',
        ),
        'bootstrap' => array(
            'wrapper' => 'Smorken\Strap\Builders\Modifiers\Wrapper\Bootstrap',
        ),
    );

    protected $data;

    protected $wrapper;

    public function __construct($strap, $type = 'base', $overrides = array())
    {
        if (!array_key_exists($type, $this->types)) {
            throw new ModException("$type is not a valid builder.");
        }
        foreach($this->types[$type] as $name => $mod) {
            $this->$name = new $mod($strap, $overrides);
        }
    }

    public function setStrap($strap)
    {
        foreach($this->types as $type => $handlers) {
            foreach($handlers as $var => $cls) {
                if (property_exists($this, $var) && $this->$var) {
                    $this->$var->setStrap($strap);
                }
            }
        }
    }

    /**
     * @return \Smorken\Strap\Builders\Modifiers\AbstractMod
     */
    public function wrapper()
    {
        return $this->wrapper;
    }
} 