<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/17/14
 * Time: 11:35 AM
 */

namespace Smorken\Strap\Helpers;


class Args
{

    protected static $common = array(
        'two' => array(
            'name' => 'req', 'options' => array()
        ),
        'three' => array(
            'name' => 'req', 'value' => null, 'options' => array()
        ),
        'select' => array(
            'name' => 'req', 'list' => array(), 'selected' => null, 'options' => array()
        ),
        'checked' => array(
            'name' => 'req', 'value' => 1, 'checked' => null, 'options' => array()
        ),
        'button' => array(
            'value' => null, 'options' => array()
        ),
    );

    public static function getArgsList()
    {
        return array(
            'text' => static::getCommon(),
            'password' => static::getCommon('two'),
            'hidden' => static::getCommon(),
            'email' => static::getCommon(),
            'url' => static::getCommon(),
            'file' => static::getCommon('two'),
            'textarea' => static::getCommon(),
            'select' => static::getCommon('select'),
            'selectRange' => array(
                'name' => 'req', 'begin' => 'req', 'end' => 'req', 'selected' => null, 'options' => array()
            ),
            'selectYear' => array(
                'name' => 'req', 'begin' => 'req', 'end' => 'req', 'selected' => null, 'options' => array()
            ),
            'selectMonth' => array(
                'name' => 'req', 'selected' => null, 'options' => array()
            ),
            'checkbox' => static::getCommon('checked'),
            'radio' => static::getCommon('checked', 'value', null),
            'reset' => static::getCommon('button', 'value', 'req'),
            'image' => array(
                'url' => 'req', 'name' => null, 'options' => array()
            ),
            'submit' => static::getCommon('button'),
            'button' => static::getCommon('button'),
            'link' => array('href' => 'req', 'value' => null, 'options' => array()),
            'input' => array('type' => 'req', 'name' => 'req', 'value' => null, 'options' => array()),
        );
    }

    public static function getArgs($type, $args)
    {
        $newargs = array();
        if (!array_key_exists($type, static::getArgsList())) {
            throw new \InvalidArgumentException("$type is not a valid type.");
        }

        $allowed = static::getArgsList()[$type];
        $index = 0;
        foreach($allowed as $key => $value) {
            $argval = isset($args[$index]) ? $args[$index] : $value;
            if ($argval == 'req') {
                throw new \InvalidArgumentException("$key is a required argument.");
            }
            $newargs[$key] = $argval;
            $index ++;
        }
        return $newargs;
    }

    protected static function getCommon($which = 'three', $override = false, $value = null)
    {
        $r = static::$common[$which];
        if ($override && isset($r[$override])) {
            $r[$override] = $value;
        }
        return $r;
    }
} 