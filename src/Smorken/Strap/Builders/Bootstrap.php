<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/17/14
 * Time: 3:17 PM
 */

namespace Smorken\Strap\Builders;

use Smorken\Strap\Parts\Wrapper;

class Bootstrap extends Base
{

    const INPUT_LG = 'input-lg';
    const INPUT_SM = 'input-sm';

    const BTN_DEFAULT = 'btn-default';
    const BTN_PRIMARY = 'btn-primary';
    const BTN_SUCCESS = 'btn-success';
    const BTN_INFO = 'btn-info';
    const BTN_WARNING = 'btn-warning';
    const BTN_DANGER = 'btn-danger';
    const BTN_LINK = 'btn-link';

    protected $types = array(
        'default',
        'horizontal',
        'inline',
    );

    protected $bootstrap = array(
        'horizontal' => array(
            'wrapper' => array(
                'fields' => array(
                    'tag' => 'div',
                    'options' => array(
                        'class' => array('col-sm-10'),
                    ),
                    'checkbox' => array(
                        'options' => array(
                            'class' => array('col-sm-offset-2'),
                        )
                    ),
                    'button' => array(
                        'options' => array(
                            'class' => array('col-sm-offset-2'),
                        )
                    )
                )
            ),
            'fields' => array(
                'options' => array(
                    'class' => array('col-sm-10'),
                )
            ),
            'label' => array(
                'options' => array(
                    'class' => array('col-sm-2', 'control-label'),
                ),
            ),
        ),
        'button' => array(
            'options' => array(
                'class' => array('btn')
            ),
        ),
    );

    protected $overrides = array(
        'outer' => array(
            'tag' => 'div',
            'options' => array(
                'class' => array('form-group'),
            ),
        ),
    );


    /*protected function horizontal_wrapper_fields($wrapper, $content)
    {
        $data = $this->bootstrap['horizontal']['wrapper']['fields'];
        $attributes = $wrapper->getAttributes();
        $attributes = $this->addClasses($data['options']['class'], $attributes);
        $w = new Wrapper($data['tag'], $attributes);
        $w->setValue($wrapper);
        return $w;
    }

    protected function horizontal_wrapper_fields_checkbox($wrapper, $content)
    {
        $data = $this->bootstrap['horizontal']['wrapper']['fields']['checkbox'];
        $attributes = $wrapper->getAttributes();
        $attributes = $this->addClasses($data['options']['class'], $attributes);
        $wrapper->setAttributes($attributes);
        $inner = $wrapper->getValue();

        $w = new Wrapper('div', array('class' => 'checkbox'));
        $w->setValue($inner);
        $wrapper->setValue($w);
        return $wrapper;
    }*/




    protected function default_wrapper_outer_container_checkbox($wrapper, $content)
    {
        $wrapper->setAttributes(array('class' => 'checkbox'));
        return $wrapper;
    }


    /*protected function horizontal_wrapper_outer_container_button($wrapper, $content)
    {
        return $this->handleHorizontalButton('<button', $wrapper, $content);
    }

    protected function horizontal_wrapper_outer_container_submit($wrapper, $content)
    {
        return $this->handleHorizontalButton('<input', $wrapper, $content);
    }

    protected function horizontal_wrapper_outer_container_reset($wrapper, $content)
    {
        return $this->handleHorizontalButton('<input', $wrapper, $content);
    }

    protected function handleHorizontalButton($type, $wrapper, $content)
    {
        $this->removeLabelWrapper($wrapper);
        $buttonwrapper = $this->recurseWrappers($wrapper, $type);
        $buttoncontainer = $this->recurseWrappers($wrapper, $buttonwrapper);
        if ($buttoncontainer) {
            $data = $this->bootstrap['horizontal']['wrapper']['fields']['checkbox'];
            $attributes = $buttoncontainer->getAttributes();
            $attributes = $this->addClasses($data['options']['class'], $attributes);
            $buttoncontainer->setAttributes($attributes);
        }
        return $wrapper;
    }

    protected function default_wrapper_outer_container_button($wrapper, $content)
    {
        return $this->cleanupButtonWrappers($wrapper, '<button');
    }

    protected function default_wrapper_outer_container_submit($wrapper, $content)
    {
        return $this->cleanupButtonWrappers($wrapper, '<input');
    }

    protected function default_wrapper_outer_container_reset($wrapper, $content)
    {
        return $this->cleanupButtonWrappers($wrapper, '<input');
    }

    protected function cleanupButtonWrappers($wrapper, $search)
    {
        $result = $this->recurseWrappers($wrapper, $search);
        if ($result) {
            return $result;
        }
        return $wrapper;
    }

    protected function removeLabelWrapper($wrapper)
    {
        $result = $this->recurseWrappersByTag($wrapper, 'label');
        if ($result) {
            $poss_values = $result->getValue();
            foreach ($poss_values as $i => $value) {
                if ($value instanceof Wrapper && $value->getTag() == 'label') {
                    unset($poss_values[$i]);
                }
            }
            $result->setValue($poss_values);
        }
    }*/

}