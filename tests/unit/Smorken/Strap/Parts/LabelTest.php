<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/17/14
 * Time: 10:33 AM
 */

namespace unit\Smorken\Strap\Parts;

use Smorken\Strap\Parts\Label;

class LabelTest extends \PHPUnit_Framework_TestCase {

    public function testConstructWithNameSetsForAttribute()
    {
        $sut = new Label('foo');
        $this->assertEquals(' for="foo"', $sut->attributesToString($sut->getAttributes()));
    }

    public function testConstructWithValue()
    {
        $sut = new Label('foo', 'Bar');
        $this->assertEquals('Bar', $sut->getValue()[0]);
    }

    public function testWrap()
    {
        $sut = new Label('foo', 'Bar');
        $expected = '<label for="foo">Bar</label>';
        $this->assertEquals($expected, $sut->wrap());
    }

    public function testWrapWithArray()
    {
        $sut = new Label('foo', array('Bar', 'Baz'));
        $expected = '<label for="foo">Bar Baz</label>';
        $this->assertEquals($expected, $sut->wrap());
    }

    public function testWrapWithAttributes()
    {
        $sut = new Label('foo', 'Bar', array('class' => 'baz'));
        $expected = '<label class="baz" for="foo">Bar</label>';
        $this->assertEquals($expected, $sut->wrap());
    }
} 