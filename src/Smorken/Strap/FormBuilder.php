<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/16/14
 * Time: 6:08 PM
 */

namespace Smorken\Strap;

use Illuminate\Html\FormBuilder as Form;

class FormBuilder extends Form {

    /**
     * @return \Illuminate\Html\HtmlBuilder
     */
    public function getHtml()
    {
        return $this->html;
    }

    public function link($href, $value = null, $options = array())
    {
        $options['href'] = $href;
        $value = $value === null ? $href : $value;
        if (!isset($options['title'])) {
            $options['title'] = 'Link to ' . $value;
        }
        return '<a' . $this->html->attributes($options) . '>' . e($value) . '</a>';
    }

    /**
     * Create a form label element.
     *
     * @param  string  $name
     * @param  string  $value
     * @param  array   $options
     * @return string
     */
    public function label($name, $value = null, $options = array())
    {
        $this->labels[] = $name;
        if (!isset($options['for'])) {
            $options['for'] = $name;
        }
        $options = $this->html->attributes($options);

        $value = e($this->formatLabel($name, $value));

        return '<label' . $options.'>'.$value.'</label>';
    }

    /**
     * Format the label value.
     *
     * @param  string  $name
     * @param  string|null  $value
     * @return string
     */
    public function _formatLabel($name, $value)
    {
        return $this->formatLabel($name, $value);
    }

    /**
     * Determine if the old input is empty.
     *
     * @return bool
     */
    public function oldInputIsEmpty()
    {
        if (!isset($this->session)) {
            return true;
        }
        return (isset($this->session) && count($this->session->getOldInput()) == 0);
    }
} 