<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/17/14
 * Time: 8:51 AM
 */

namespace Smorken\Strap;

use Smorken\Strap\Builders\Bootstrap;
use Smorken\Strap\Builders\BuilderException;
use Smorken\Strap\Helpers\Args;
use Illuminate\Support\Facades\App;

class Strap
{

    /**
     * @var \Smorken\Strap\FormBuilder
     */
    protected $formBuilder;

    protected $instances = array(
        'base' => '\Smorken\Strap\Builders\Base',
        'bootstrap' => '\Smorken\Strap\Builders\Bootstrap',
    );

    protected $instanceClass = '\Smorken\Strap\Builders\Base';

    protected $field = array('type' => 'text', 'args' => array());

    protected $name;

    protected $label = array('label' => '', 'options' => array());

    protected $value = null;

    protected $checked = false;

    protected $help = array('text' => '', 'options' => array());

    protected $error = array('text' => '', 'options' => array());

    protected $inside = false;

    protected $type = 'default';

    protected $label_pos = 'left';

    protected $select_list = array();

    protected $override_instance = null;

    protected $wrap = true;

    protected $isGrouped = false;

    protected $buttonify = false;

    protected $group = array();

    protected $auto = array(
        'hidden' => array(
            'label' => false,
            'wrap' => false,
        ),
        'button' => array(
            'label' => false,
            'buttonify' => true,
        ),
        'submit' => array(
            'label' => false,
            'buttonify' => true,
        ),
        'reset' => array(
            'label' => false,
            'buttonify' => true,
        ),
    );

    protected $reset = array(
        'field' => array('type' => 'text', 'args' => array()),
        'name' => null,
        'label' => array('label' => '', 'options' => array()),
        'value' => null,
        'checked' => false,
        'help' => array('text' => '', 'options' => array()),
        'error' => array('text' => '', 'options' => array()),
        'inside' => false,
        'select_list' => array(),
        'buttonify' => false,
        'group' => array(),
        'isGrouped' => false,
    );

    public function __construct(FormBuilder $formBuilder, $instanceClass = null)
    {
        $this->setFormBuilder($formBuilder);
        if ($instanceClass !== null) {
            $this->instanceClass = $instanceClass;
        }
    }

    public function getErrors()
    {
        if (!$this->error['text'] && $this->name) {
            $session = App::make('session');
            if ($session) {
                if ($session->has('errors')) {
                    $errors = $session->get('errors');
                    $this->error['text'] = $errors->first($this->name);
                }
            }
        }
    }

    /**
     * Open up a new HTML form.
     *
     * @param  array $options
     * @return string
     */
    public function open(array $options = array())
    {
        return $this->getFormBuilder()->open($options);
    }

    /**
     * Create a new model based form builder.
     *
     * @param  mixed $model
     * @param  array $options
     * @return string
     */
    public function model($model, $options = array())
    {
        $this->getFormBuilder()->setModel($model);

        return $this->open($options);
    }

    public function close()
    {
        return $this->getFormBuilder()->close();
    }

    public function __toString()
    {
        $out = $this->toWrapper();
        return (string)$out;
    }

    public function toWrapper()
    {
        $instance = $this->instance();
        $out = $instance->build($this->group);

        return $out;
    }

    public function instance()
    {
        if (!$this->override_instance) {
            $instance = new $this->instanceClass();
            $instance->init($this);
        } else {
            if (!array_key_exists($this->override_instance, $this->instances)) {
                throw new BuilderException("{$this->override_instance} is not a valid builder.");
            }
            $class = $this->instances[$this->override_instance];
            $instance = new $class();
            $instance->init($this);
            $this->override_instance = null;
        }
        $instance->setFormBuilder($this->formBuilder);

        return $instance;
    }

    public function resetToDefaults()
    {
        foreach ($this->reset as $m => $value) {
            $this->$m = $value;
        }
    }

    public function __call($method, $args)
    {
        return $this->startNew($method, $args);
    }

    public function number($name, $min = null, $max = null)
    {
        $method = 'input';
        $args = array(
            'number',
            $name,
            null,
            array(),
        );
        if ($min !== null) {
            $args[3]['min'] = $min;
        }
        if ($max !== null) {
            $args[3]['max'] = $max;
        }
        return $this->startNew($method, $args);
    }

    public function startNew($method, $args)
    {
        $this->resetToDefaults();
        $args = Args::getArgs($method, $args);
        $this->name = isset($args['name']) ? $args['name'] : null;
        $this->field['type'] = $method;
        $this->field['args'] = $args;
        $this->getErrors();
        if (isset($this->auto[$method])) {
            $me = clone $this;
            foreach ($this->auto[$method] as $call => $value) {
                $me->$call($value);
            }
            return $me;
        }
        return clone $this;
    }

    public function getField()
    {
        return $this->field;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getHelp()
    {
        return $this->help;
    }

    public function getError()
    {
        return $this->error;
    }

    public function getInside()
    {
        return $this->inside;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getData()
    {
        return $this->select_list;
    }

    public function getChecked()
    {
        return $this->checked;
    }

    public function getLabelPosition()
    {
        return $this->label_pos;
    }

    public function getWrap()
    {
        return $this->wrap;
    }

    public function getButtonify()
    {
        return $this->buttonify;
    }

    public function getGroup()
    {
        if ($this->group) {
            return $this->group;
        }
        return false;
    }

    public function getIsGrouped()
    {
        return $this->isGrouped;
    }

    public function getBuilderName()
    {
        $builder = $this->override_instance ? $this->override_instance : $this->instanceClass;
        if (array_key_exists($builder, $this->instances)) {
            return $builder;
        }
        $key = array_search($builder, $this->instances);
        return $key;
    }

    public function isRight()
    {
        return $this->label_pos == 'right';
    }

    public function isLeft()
    {
        return $this->label_pos == 'left';
    }

    public function group($elements = array())
    {
        $this->group = $elements;
        if ($this->label['label'] == '') {
            $this->label['label'] = false;
        }
        return $this;
    }

    public function buttonify($type = Bootstrap::BTN_DEFAULT)
    {
        $this->buttonify = $type;
        return $this;
    }
    public function wrap($wrap = true)
    {
        $this->wrap = $wrap;
        return $this;
    }

    public function data($list, $value = null, $text = null)
    {
        if ($list instanceof \Illuminate\Database\Eloquent\Collection) {
            $vk = $value ? : 'id';
            foreach($list as $l) {
                $this->select_list[$l->$vk] = ($text ? $l->$text : (string) $l);
            }
        }
        else {
            if ($value && $text) {
                foreach($list as $l) {
                    $this->select_list[$l[$value]] = $l[$text];
                }
            }
            else {
                $this->select_list = $list;
            }
        }
        return $this;
    }

    public function right()
    {
        $this->label_pos = 'right';
        return $this;
    }

    public function left()
    {
        $this->label_pos = 'left';
        return $this;
    }

    public function type($type = 'default')
    {
        $this->type = $type;
        return $this;
    }

    public function help($text)
    {
        $this->help['text'] = $text;
        return $this;
    }

    public function error($text)
    {
        $this->error['text'] = $text;
        return $this;
    }

    public function inside($inside = true)
    {
        $this->inside = $inside;
        $this->label_pos = 'right';
        return $this;
    }

    public function label($label, $options = array())
    {
        $this->label['label'] = $label;
        $this->label['options'] = $options;
        return $this;
    }

    public function value($value)
    {
        if (isset($this->field['args']['value'])) {
            $this->field['args']['value'] = $value;
        }
        $this->value = $value;
        return $this;
    }

    public function options($options = array()) {
        $curr = array();
        if (!is_array($options)) {
            $options = array($options);
        }
        if (isset($this->field['args']['options'])) {
            $curr = $this->field['args']['options'];
        }
        foreach($options as $key => $value) {
            if (isset($curr[$key]) && $key == 'class') {
                $curr[$key] = $value . ' ' . $options[$key];
            }
            else {
                $curr[$key] = $options[$key];
            }
        }
        $this->field['args']['options'] = $curr;
        return $this;
    }

    public function checked($checked = true)
    {
        $this->checked = $checked;
        return $this;
    }

    public function builder($builder)
    {
        $this->override_instance = strtolower($builder);
        return $this;
    }

    public function isGrouped($grouped = true)
    {
        $this->isGrouped = $grouped;
        return $this;
    }

    public function getFormBuilder()
    {
        return $this->formBuilder;
    }

    public function setFormBuilder($formBuilder)
    {
        $this->formBuilder = $formBuilder;
    }

    public function setInstanceClass($class)
    {
        $this->instanceClass = $class;
    }
} 