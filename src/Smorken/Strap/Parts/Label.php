<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/17/14
 * Time: 10:25 AM
 */

namespace Smorken\Strap\Parts;


class Label extends Wrapper {



    protected $tag = 'label';

    public function __construct($name, $value = null, $attributes = null)
    {
        if ($attributes !== null) {
            $this->setAttributes($attributes);
        }
        $this->addAttribute($name, 'for');

        $this->setValue($value);
    }


}