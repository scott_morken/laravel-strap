<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/21/14
 * Time: 9:40 AM
 */

namespace Smorken\Strap\Parts;

use Smorken\Strap\FormBuilder;
use Smorken\Strap\Helpers\Args;
use Smorken\Strap\Builders\BuilderException;

class Input extends Wrapper {

    protected $args;

    protected $type;

    /**
     * @var FormBuilder
     */
    protected $formBuilder;

    protected $tag = '';

    /**
     * @param FormBuilder $formBuilder
     * @param null $type
     * @param $args
     */
    public function __construct(FormBuilder $formBuilder, $type, $args)
    {
        if (!array_key_exists('options', $args)) {
            $args = Args::getArgs($type, $args);
        }
        $this->setAttributes($args['options']);
        $this->setFormBuilder($formBuilder);
        $this->setType($type);
        $this->setArgs($args);

    }

    public function wrap($html = null, $attributes = null)
    {
        if ($html !== null) {
            $this->setValue($html);
        }
        if ($attributes !== null) {
            $this->setAttributes($attributes);
        }
        $this->args['options'] = $this->attributes;
        if (!method_exists($this->getFormBuilder(), $this->getType())) {
            throw new BuilderException("{$this->getType()} is not a valid form element.");
        }
        return call_user_func_array(array($this->getFormBuilder(), $this->getType()), $this->args);
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return FormBuilder
     */
    public function getFormBuilder()
    {
        return $this->formBuilder;
    }

    /**
     * @param FormBuilder $formBuilder
     */
    public function setFormBuilder($formBuilder)
    {
        $this->formBuilder = $formBuilder;
    }

    public function setArgs($args)
    {
        $this->args = $args;
    }

    public function getArgs()
    {
        return $this->args;
    }

    public function setArg($key, $value)
    {
        $this->args[$key] = $value;
    }

    public function getArg($key, $default = null)
    {
        if (isset($this->args[$key])) {
            return $this->args[$key];
        }
        return $default;
    }

    public function getId()
    {
        return $this->formBuilder->getIdAttribute($this->getArg('name'), $this->getArg('options'));
    }
} 