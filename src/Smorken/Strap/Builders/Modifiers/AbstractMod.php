<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/18/14
 * Time: 12:57 PM
 */

namespace Smorken\Strap\Builders\Modifiers;

use Smorken\Strap\Parts\Wrapper;

abstract class AbstractMod
{

    /**
     * array of handlers for mods
     * 'attr_mod' => array(
     *   {{for}} => array( //field, label, help, error
     *     {{field_type}} => array( //text, checkbox, ...
     *     ),
     *   ),
     *   {{output_type}} => array( //default, horizontal, inline...
     *     {{for}} => array(
     *       {{field_type}} => array(
     *       ),
     *     ),
     *   ),
     * ),
     * 'wrap_mod' => array(
     *   {{for}} => array( //field, label, help, error
     *     {{field_type}} => array( //text, checkbox, ...
     *     ),
     *   ),
     *   {{output_type}} => array( //default, horizontal, inline...
     *     {{for}} => array(
     *       {{field_type}} => array(
     *       ),
     *     ),
     *   ),
     *   'group' => array(  // *1
     *     {{field_type}} => array( // *2
     *     ),
     *     {{col_type}} => array( //col, col-1 & col-2, col-inside  *3
     *       {{field_type}} => array( // *6
     *       )
     *     ),
     *     {{output_type}} => array( // *4
     *       {{col_type}} => array( // *5
     *         {{field_type}} => array( // *7
     *         ),
     *       ),
     *     ),
     *
     *   )
     *
     * ),
     * accessed via dot notation:
     * for.field_type returns method from {{for}}[{{field_type}}]
     * @var array
     */
    protected $handlers = array();

    /**
     * overrides default handlers if you don't want to recreate the entire array
     * or want to share most functionality among children
     * @var array
     */
    protected $childHandlers = array();

    /**
     * array of overrides that overwrite values in $this->defaults
     * @var array
     */
    protected $overrides = array();

    protected $defaults = array();

    /**
     * @var \Smorken\Strap\Strap
     */
    protected $strap;

    public function __construct($strap, $overrides = array())
    {
        $this->strap = $strap;

        $this->overrides = array_replace_recursive($this->overrides, $overrides);
        $this->defaults = array_replace_recursive($this->defaults, $this->overrides);

        $this->handlers = $this->array_replace_recursive($this->handlers, $this->childHandlers);
    }

    public function setStrap($strap)
    {
        $this->strap = $strap;
    }

    public function getHandlers()
    {
        return $this->handlers;
    }

    protected function array_replace_recursive($array, $array1)
    {
        

        // handle the arguments, merge one by one
        $args = func_get_args();
        $array = $args[0];
        if (!is_array($array))
        {
            return $array;
        }
        for ($i = 1; $i < count($args); $i++)
        {
            if (is_array($args[$i]))
            {
                $array = $this->recurse($array, $args[$i]);
            }
        }
        return $array;
    }

    function recurse($array, $array1)
    {
        foreach ($array1 as $key => $value)
        {
            if ($key === 'method') {
                if (isset($array[$key])) {
                    $v = $array[$key];
                    if (!is_array($v)) {
                        $v = array($v);
                    }
                    if (!is_array($value)) {
                        $value = array($value);
                    }
                    $value = array_merge($v, $value);
                }
            }
            // create new key in $array, if it is empty or not an array
            if (!is_numeric($key) && (!isset($array[$key]) || (isset($array[$key]) && !is_array($array[$key]))))
            {
                $array[$key] = array();
            }

            // overwrite the value in the base array
            if (is_array($value))
            {
                $value = $this->recurse($array[$key], $value);
            }
            $array[$key] = $value;
        }
        return $array;
    }

    /**
     * @param $handler
     * @param int $defaultReturnArg
     * @param ...
     * @throws ModException
     * @internal param $defaultReturn
     * @return mixed
     */
    public function mod($handler, $defaultReturnArg = 0)
    {
        $args = func_get_args();
        if (count($args) < 3) {
            throw new ModException("No arguments provided for the modifier.");
        }
        $args = array_slice($args, 2);
        $result = $args[$defaultReturnArg];
        $location = array_get($this->handlers, $handler);
        if ($location && isset($location['method'])) {
            $methods = $location['method'];
            if (!is_array($methods)) {
                $methods = array($methods);
            }
            foreach ($methods as $method) {
                if (!method_exists($this, $method)) {
                    throw new ModException("$method is not a callable modifier.");
                }
                $tempResult = call_user_func_array(array($this, $method), $args);
                if ($tempResult) {
                    $args[$defaultReturnArg] = $result = $tempResult;
                }
            }
        }
        return $result;
    }

    protected function getDefault($key, $default = array())
    {
        return array_get($this->defaults, $key, $default);
    }

    /**
     * @return \Smorken\Strap\Strap
     */
    protected function strap()
    {
        return $this->strap;
    }

    public function addClasses($classes, $attributes)
    {
        return $this->addToAttributes('class', $classes, $attributes);
    }

    public function addToAttributes($key, $items, $attributes)
    {
        $current_items = isset($attributes[$key]) ? $attributes[$key] : '';
        $add = array();
        foreach ($items as $item) {
            if (!preg_match("/\\b$item|$item\\b|^$item$/i", $current_items)) {
                $add[] = $item;
            }
        }
        if ($add) {
            if ($current_items) {
                $add[] = $current_items;
            }
            $attributes[$key] = implode(' ', $add);
        }
        return $attributes;
    }

    public function removeClasses($classes, $attributes)
    {
        return $this->removeFromAttributes('class', $classes, $attributes);
    }

    public function removeFromAttributes($key, $items, $attributes)
    {
        $current_items = isset($attributes[$key]) ? $attributes[$key] : '';
        $current_items = explode(' ', $current_items);
        foreach ($items as $item) {
            if (($i = array_search($item, $current_items)) !== false) {
                unset($current_items[$i]);
            }
        }
        if (!$current_items) {
            unset($attributes[$key]);
        } else {
            $attributes[$key] = implode(' ', $current_items);
        }
        return $attributes;
    }

    protected function recurseWrappers($wrapper, $search = '<input', $exact = false, $parent = null)
    {
        if (!$wrapper instanceof Wrapper && !$search instanceof Wrapper) {
            if ($exact) {
                if ($search === $wrapper) {
                    return $parent;
                }
            } else {
                if (str_contains($wrapper, $search) !== false) {
                    return $parent;
                }
            }
            return false;
        }
        if ($search instanceof Wrapper) {
            if ($wrapper === $search) {
                return $parent;
            }
        }
        if ($wrapper instanceof Wrapper) {
            foreach ($wrapper->getValue() as $w) {
                $r = $this->recurseWrappers($w, $search, $exact, $wrapper);
                if ($r) {
                    return $r;
                }
            }
        }
        return false;
    }

    protected function recurseWrappersByTag($wrapper, $search = 'label', $parent = null)
    {
        if (!$wrapper instanceof Wrapper) {
            return false;
        }
        $tag = $wrapper->getTag();
        if ($tag == $search) {
            return $parent;
        }
        foreach ($wrapper->getValue() as $w) {
            $r = $this->recurseWrappersByTag($w, $search, $wrapper);
            if ($r) {
                return $r;
            }
        }
        return false;
    }

    protected function recurseWrappersByType($wrapper, $search = 'Input', $parent = null)
    {
        if (!$wrapper instanceof Wrapper) {
            return false;
        }
        $type = class_basename($wrapper);
        if ($type == $search) {
            return $parent;
        }
        foreach ($wrapper->getValue() as $w) {
            $r = $this->recurseWrappersByType($w, $search, $wrapper);
            if ($r) {
                return $r;
            }
        }
        return false;
    }
} 