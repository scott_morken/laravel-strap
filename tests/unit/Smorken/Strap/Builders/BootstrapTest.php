<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/17/14
 * Time: 3:25 PM
 */

namespace unit\Smorken\Strap\Builders;
use Mockery as m;
use Illuminate\Html\HtmlBuilder;
use Smorken\Strap\FormBuilder;
use Smorken\Strap\Strap;
use Illuminate\Support\Facades\App;

class BootstrapTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \Smorken\Strap\Builders\Base
     */
    protected $sut;

    public function setUp()
    {
        $html = new HtmlBuilder();
        $urlmock = m::mock('Illuminate\Routing\UrlGenerator');
        $form = new FormBuilder($html, $urlmock, 'xyz');
        $this->sut = new \Smorken\Strap\Builders\Bootstrap();
        $this->sut->setFormBuilder($form);
    }

    public function tearDown()
    {
        m::close();
    }

    public function testbuildField()
    {
        $this->sut->setStrap($this->createStrap()->text('name'));
        $this->sut->buildField();
        $fields = $this->sut->getField();
        $expected = '<input class="form-control" name="name" type="text">';
        $this->assertEquals($expected, (string) $fields);
    }

    public function testbuildFieldHorizontal()
    {
        $strap = $this->createStrap()->text('name')->type('horizontal');
        $this->sut->setStrap($strap);
        $this->sut->buildField();
        $fields = $this->sut->getField();
        $expected = '<input class="form-control" name="name" type="text">';
        $this->assertEquals($expected, (string) $fields);
    }

    public function testBuildHorizontal()
    {
        $strap = $this->createStrap()->text('name')->type('horizontal')->label('foo')
            ->help('HELP')->error('baz');
        $strap->type('horizontal');
        $this->sut->setStrap($strap);
        $w = $this->sut->build();
        $expected = '<div class="form-group"><label class="col-sm-2 control-label">foo</label> <div class="col-sm-10"><input class="form-control" name="name" type="text"> HELP <span class="text-danger">baz</span></div></div>';
        $this->assertEquals($expected, (string) $w);
    }

    public function testbuildFieldHorizontalCheckbox()
    {
        $strap = $this->createStrap()->checkbox('name')->type('horizontal');
        $this->sut->setStrap($strap);
        $this->sut->buildField();
        $fields = $this->sut->getField();
        $expected = '<input name="name" type="checkbox" value="1">';
        $this->assertEquals($expected, (string) $fields);
    }

    public function testBuildLabelHorizontal()
    {
        $strap = $this->createStrap()->text('name')->label('foo')->type('horizontal');
        $this->sut->setStrap($strap);
        $this->sut->buildLabel();
        $fields = $this->sut->getLabel();
        $expected = '<label for="name">foo</label>';
        $this->assertEquals($expected, (string) $fields);
    }

    public function testBuildLabelHorizontalInside()
    {
        $strap = $this->createStrap()->text('name')->label('foo')->type('horizontal')->inside();
        $this->sut->setStrap($strap);
        $this->sut->buildField();
        $this->sut->buildLabel();
        $fields = $this->sut->getLabel();
        $expected = '<label for="name"><input class="form-control" name="name" type="text"> foo</label>';
        $this->assertEquals($expected, (string) $fields);
    }

    public function testBuildLabelHorizontalInsideCheckbox()
    {
        $strap = $this->createStrap()->checkbox('name')->type('horizontal')->inside()->label('foo');
        $this->sut->setStrap($strap);
        $this->sut->buildField();
        $this->sut->buildLabel();
        $fields = $this->sut->getLabel();
        $expected = '<label for="name"><input name="name" type="checkbox" value="1"> foo</label>';
        $this->assertEquals($expected, (string) $fields);
    }

    public function testBuildLabelHorizontalInsideCheckboxLeft()
    {
        $strap = $this->createStrap()->checkbox('name')->type('horizontal')->inside()->label('foo')->left();
        $this->sut->setStrap($strap);
        $this->sut->buildField();
        $this->sut->buildLabel();
        $fields = $this->sut->getLabel();
        $expected = '<label for="name">foo <input name="name" type="checkbox" value="1"></label>';
        $this->assertEquals($expected, (string) $fields);
    }

    public function testBuildCheckboxInsideDefault()
    {
        $strap = $this->createStrap()->checkbox('name')->inside()->label('foo');
        $this->sut->setStrap($strap);

        $w = $this->sut->build();
        $expected = '<div class="checkbox"><label for="name"><input name="name" type="checkbox" value="1"> foo</label>  </div>';
        $this->assertEquals($expected, (string) $w);
    }

    public function testBuildCheckboxHorizontalInside()
    {
        $strap = $this->createStrap()->checkbox('name')->inside()->label('foo')->type('horizontal');
        $this->sut->setStrap($strap);
        $o = $this->sut->build();
        $expected = '<div class="form-group"><div class="col-sm-offset-2 col-sm-10"><div class="checkbox"><label for="name"><input name="name" type="checkbox" value="1"> foo</label></div>  </div></div>';
        $this->assertEquals($expected, (string) $o);
    }

    public function testBuildRadioInsideDefault()
    {
        $strap = $this->createStrap()->radio('name')->inside()->label('foo');
        $this->sut->setStrap($strap);

        $w = $this->sut->build();
        $expected = '<div class="radio"><label for="name"><input name="name" type="radio" value="name"> foo</label>  </div>';
        $this->assertEquals($expected, (string) $w);
    }

    public function testBuildHidden()
    {
        $strap = $this->createStrap();
        $o = $strap->hidden('name')->label(false)->wrap(false);
        $this->sut->setStrap($o);
        $w = $this->sut->build();
        $expected = '<input class="form-control" name="name" type="hidden">';
        $this->assertEquals($expected, (string) $w);
    }

    public function testBuildButton()
    {
        $strap = $this->createStrap();
        $o = $strap->button('name');
        $this->sut->setStrap($o);
        $this->sut->buildField();

        $w = $this->sut->getField();
        $expected = '<button class="btn btn-default" type="button">name</button>';
        $this->assertEquals($expected, (string) $w);
    }

    public function testBuildButtonHorizontal()
    {
        $strap = $this->createStrap()->button('name')->type('horizontal')->help('HELP')->error('baz');
        $this->sut->setStrap($strap);
        $w = $this->sut->build();
        $expected = '<div class="form-group"><div class="col-sm-2"></div> <div class="col-sm-10"><button class="btn btn-default" type="button">name</button> HELP <span class="text-danger">baz</span></div></div>';
        $this->assertEquals($expected, (string) $w);
    }

    public function testBuildGroup()
    {
        $html = new HtmlBuilder();
        $urlmock = m::mock('Illuminate\Routing\UrlGenerator');
        $form = new FormBuilder($html, $urlmock, 'xyz');
        $strap = new Strap($form);

        $f1 = $this->createStrap()->text('name')->label('foo')->help('HELP')->error('baz');
        $f2 = $this->createStrap()->text('name2');
        $strap->group(array($f1, $f2))->label(false);
        $this->sut->setStrap($strap);
        $w = $this->sut->build(array($f1, $f2));
        $expected = '<div class="form-group"> <label for="name">foo</label> <input class="form-control" name="name" type="text"> HELP <span class="text-danger">baz</span> <label for="name2">Name2</label> <input class="form-control" name="name2" type="text">    </div>';
        $this->assertEquals($expected, (string) $w);
    }

    public function testBuildButtonGroup()
    {
        $html = new HtmlBuilder();
        $urlmock = m::mock('Illuminate\Routing\UrlGenerator');
        $form = new FormBuilder($html, $urlmock, 'xyz');
        $strap = new Strap($form);

        $f1 = $this->createStrap()->submit('Save')->help(null)->error(null);
        $f2 = $this->createStrap()->button('Cancel')->help(null)->error(null);
        $strap->group(array($f1, $f2));
        $this->sut->setStrap($strap);
        $w = $this->sut->build(array($f1, $f2));
        $expected = '<div class="form-group">  <input class="btn btn-default" type="submit" value="Save">    <button class="btn btn-default" type="button">Cancel</button>    </div>';
        $this->assertEquals($expected, (string) $w);
    }

    public function testBuildCheckboxGroupAddsCheckboxInline()
    {
        $html = new HtmlBuilder();
        $urlmock = m::mock('Illuminate\Routing\UrlGenerator');
        $form = new FormBuilder($html, $urlmock, 'xyz');
        $strap = new Strap($form);

        $f1 = $this->createStrap()->checkbox('name')->inside();
        $f2 = $this->createStrap()->checkbox('name2')->inside();
        $strap->group(array($f1, $f2))->label('Checkboxes');
        $this->sut->setStrap($strap);
        $w = $this->sut->build(array($f1, $f2));
        $expected = '<div class="form-group"><label for="">Checkboxes</label> <label for="name" class="checkbox-inline"><input name="name[]" type="checkbox" value="1"> Name</label>   <label for="name2" class="checkbox-inline"><input name="name2[]" type="checkbox" value="1"> Name2</label>    </div>';
        $this->assertEquals($expected, (string) $w);
    }

    public function testLinkButtonify()
    {
        $strap = $this->createStrap()->link('www.google.com', 'Google')->buttonify('btn-danger');
        $this->sut->setStrap($strap);
        $this->sut->buildField();
        $w = $this->sut->getField();
        $expected = '<a class="btn btn-danger" href="www.google.com" title="Link to Google">Google</a>';
        $this->assertEquals($expected, (string) $w);
    }

    public function testHiddenBeforeResetsPropertiesToPrevious()
    {
        $html = new HtmlBuilder();
        $urlmock = m::mock('Illuminate\Routing\UrlGenerator');
        $form = new FormBuilder($html, $urlmock, 'xyz');
        $strap = new Strap($form);
        $o = $strap->hidden('testhidden');
        $o2 = $strap->checkbox('testchk')->label('Label');
        $this->assertNotSame($o, $o2);
        $this->sut->setStrap($o);
        $w = $this->sut->build();
        $expected = '<input class="form-control" name="testhidden" type="hidden">';
        $this->assertEquals($expected, (string) $w);
        $this->sut->setStrap($o2);
        $w2 = $this->sut->build();
        $expected = '<div class="checkbox"><label for="testchk">Label</label> <input name="testchk" type="checkbox" value="1">  </div>';
        $this->assertEquals($expected, (string) $w2);
    }

    protected function createStrap()
    {
        App::shouldReceive('make')
            ->with('session')
            ->andReturn(false);
        $html = new HtmlBuilder();
        $urlmock = m::mock('Illuminate\Routing\UrlGenerator');
        $form = new FormBuilder($html, $urlmock, 'xyz');
        $strap = new Strap($form);

        return $strap;
    }

}