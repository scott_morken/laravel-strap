<?php namespace Smorken\Strap;

use Illuminate\Support\ServiceProvider;
use Smorken\Strap\FormBuilder;

class StrapServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = true;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('smorken/strap');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
        $this->app->bindShared('form', function($app)
        {
            $form = new FormBuilder($app['html'], $app['url'], $app['session.store']->getToken());

            return $form->setSessionStore($app['session.store']);
        });

		$this->app['strap'] = $this->app->share(function($app) {
            $form = new Strap($app['form'], '\Smorken\Strap\Builders\Bootstrap');

            return $form;
        });
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('strap', 'form');
	}

}
