<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/18/14
 * Time: 12:50 PM
 */

namespace Smorken\Strap\Builders\Modifiers\Wrapper;

use Smorken\Strap\Builders\Modifiers\AbstractMod;
use Smorken\Strap\Parts\Input;

class Base extends AbstractMod
{

    protected $handlers = array(
        'attr_mod' => array(
            'field' => array(
                'checkbox' => array(
                    'method' => 'handleChecked',
                ),
                'radio' => array(
                    'method' => 'handleChecked',
                ),
                'select' => array(
                    'method' => 'handleSelectData',
                ),
            ),
            'label' => array(
            ),
            'default' => array(
                'field' => array(
                ),
                'label' => array(),
                'help' => array(),
                'error' => array(),
            ),
        ),
        'wrap_mod' => array(
            'label' => array(
            )
        ),

    );

    public function handleMulti($wrapper)
    {
        if ($this->strap()->getIsGrouped() && $wrapper instanceof Input) {
            $name = $wrapper->getArg('name');
            if ($name && substr($name, 0, -1) !== ']') {
                $wrapper->setArg('name', $name . '[]');
            }
        }
        return $wrapper;
    }

    public function handleSelectData($wrapper)
    {
        $wrapper->setArg('list', $this->strap()->getData());
    }

    public function handleChecked($wrapper)
    {
        if ($this->strap()->getChecked()) {
            $wrapper->addAttribute('checked');
        }
        return $wrapper;
    }

}