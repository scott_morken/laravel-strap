<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/17/14
 * Time: 9:55 AM
 */

namespace unit\Smorken\Strap\Parts;

use Smorken\Strap\Parts\Wrapper;

class WrapperTest extends \PHPUnit_Framework_TestCase
{

    public function testConstructorOverride()
    {
        $sut = new Wrapper('li');
        $this->assertEquals('li', $sut->getTag());
    }

    public function testConstructorSetEmpty()
    {
        $sut = new Wrapper(null);
        $this->assertEquals('', $sut->getTag());
    }

    public function testSetEmpty()
    {
        $sut = new Wrapper();
        $sut->setEmpty();
        $this->assertEquals('', $sut->getTag());
    }

    public function testSetTag()
    {
        $sut = new Wrapper();
        $sut->setTag('foo');
        $this->assertEquals('foo', $sut->getTag());
    }

    public function testSetAttributes()
    {
        $sut = new Wrapper();
        $sut->setAttributes('bar');
        $this->assertEquals('bar', $sut->getAttributes());
    }

    public function testStart()
    {
        $sut = new Wrapper();
        $o = $sut->start();
        $this->assertEquals('<div>', $o);
    }

    public function testStartWithAttributes()
    {
        $sut = new Wrapper();
        $sut->setAttributes('bar');
        $this->assertEquals('<div bar="bar">', $sut->start());
    }

    public function testStartWithAttributesNoDoubleSpace()
    {
        $sut = new Wrapper();
        $sut->setAttributes(' bar');
        $this->assertEquals('<div bar="bar">', $sut->start());
    }

    public function testEnd()
    {
        $sut = new Wrapper();
        $this->assertEquals('</div>', $sut->end());
    }

    public function testWrap()
    {
        $html = 'test';
        $sut = new Wrapper();
        $this->assertEquals('<div>test</div>', $sut->wrap($html));
    }

    public function testWrapWithAttributes()
    {
        $html = 'test';
        $atts = array('foo' => 'bar');
        $expected = "<div foo=\"bar\">$html</div>";
        $sut = new Wrapper();
        $this->assertEquals($expected, $sut->wrap($html, $atts));
    }

    public function testWrapperWrapsSelf()
    {
        $sut = new Wrapper();
        $wrapper = new Wrapper('foo');
        $sut->setValue($wrapper);
        $expected = '<div><foo></foo></div>';
        $this->assertEquals($expected, $sut->wrap());
    }

    public function testWrapperWrapsSelfWithText()
    {
        $sut = new Wrapper();
        $w1 = new Wrapper('foo');
        $sut->setValue(array($w1, 'baz'));
        $w1->setValue('bar');
        $expected = '<div><foo>bar</foo> baz</div>';
        $this->assertEquals($expected, $sut->wrap());
    }

    public function testWrapperValuePrepend()
    {
        $sut = new Wrapper();
        $sut->setValue('foo');
        $sut->addValue('bar', false);
        $this->assertEquals('bar', $sut->getValue()[0]);
    }
}