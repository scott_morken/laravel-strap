<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/18/14
 * Time: 2:22 PM
 */

namespace Smorken\Strap\Builders\Modifiers\Wrapper;

use Smorken\Strap\Parts\Label;
use Smorken\Strap\Parts\Wrapper;
use Smorken\Strap\Parts\Input;

class Bootstrap extends Base
{

    protected $childHandlers = array(
        'attr_mod' => array(
            'field' => array(
                'method' => 'addFormControl',
                'checkbox' => array(
                    'method' => 'removeFormControl',
                ),
                'radio' => array(
                    'method' => 'removeFormControl',
                ),
                'link' => array(
                    'method' => array('checkButtonClasses', 'removeFormControl'),
                ),
                'button' => array(
                    'method' => array('addButtonClasses', 'removeFormControl'),
                ),
                'submit' => array(
                    'method' => array('addButtonClasses', 'removeFormControl'),
                ),
                'reset' => array(
                    'method' => array('addButtonClasses', 'removeFormControl'),
                )
            ),
            'inline' => array(
                'label' => array(
                    'method' => 'addScreenReaderClass',
                ),
            ),
            'inside' => array(
                'label' => array(
                    'checkbox' => array(
                        'method' => array('addCheckboxInlineClassIfGrouped')
                    ),
                    'radio' => array(
                        'method' => array('addRadioInlineClassIfGrouped'),
                    )
                ),
            ),
        ),
        'wrap_mod' => array(
            'error' => array(
                'method' => 'addErrorWrapper',
            ),
            'label' => array(
                'inside' => array(
                    'checkbox' => array(
                        'method' => 'wrapCheckboxIfHorizontal',
                    ),
                    'radio' => array(
                        'method' => 'wrapRadioIfHorizontal',
                    )
                ),
            ),
            'horizontal' => array(
                'checkbox' => array(
                    'method' => 'wrapCheckbox',
                ),
                'radio' => array(
                    'method' => 'wrapRadio',
                ),
            ),
            'default' => array(
                'outer' => array(
                    'checkbox' => array('method' => array('removeFormGroup', 'addCheckboxClass'),
                    ),
                    'radio' => array('method' => array('removeFormGroup', 'addRadioClass'),
                    ),
                ),
            ),
            'outer' => array(
                'method' => 'addFormGroup',
                'hidden' => array(
                    'method' => array('stripWrappers', 'removeExtraValuesHidden'),
                ),
                'default' => array(
                    'button' => array(
                        'method' => 'stripWrappers',
                    ),
                    'submit' => array(
                        'method' => 'stripWrappers',
                    ),
                    'reset' => array(
                        'method' => 'stripWrappers',
                    ),
                    'link' => array(
                        'method' => 'stripWrappers',
                    )
                ),
            ),
            'group' => array(
                'button' => array(
                    'method' => 'stripLabel',
                ),
                'submit' => array(
                    'method' => 'stripLabel',
                ),
                'reset' => array(
                    'method' => 'stripLabel',
                ),
                'link' => array(
                    'method' => 'stripLabel',
                ),
                'default' => array(
                    'col' => array(
                        'checkbox' => array(),
                    ),
                ),
                'horizontal' => array(
                    'col-1' => array(
                        'method' => 'addColOneClasses'
                    ),
                    'col-2' => array(
                        'method' => 'addColTwoClasses',
                    ),
                    'col-inside' => array(
                        'method' => 'addOffsetWrapper',
                    )
                )
            )
        ),
    );

    protected $overrides = array(
        'outer' => array(
            'options' => array(
                'class' => array('form-group'),
            ),
        ),
        'button' => array(
            'options' => array(
                'class' => array('btn'),
            ),
        ),
        'field' => array(
            'options' => array(
                'class' => array('form-control'),
            ),
        ),
        'error' => array(
            'options' => array(
                'class' => array('text-danger'),
            ),
        ),
        'label' => array(
            'grouped' => array(
                'checkbox' => array(
                    'options' => array(
                        'class' => array('checkbox-inline'),
                    ),
                ),
                'radio' => array(
                    'options' => array(
                        'class' => 'radio-inline',
                    )
                )
            ),
        ),
        'wrapper' => array(
            'checkbox' => array(
                'options' => array(
                    'class' => array('checkbox'),
                ),
            ),
            'radio' => array(
                'options' => array(
                    'class' => array('radio'),
                ),
            ),
        ),
        'horizontal' => array(
            'label' => array(
                'options' => array(
                    'class' => array('col-sm-2', 'control-label'),
                ),
            ),
            'col-1' => array(
                'options' => array(
                    'class' => array('col-sm-2'),
                )
            ),
            'col-2' => array(
                'options' => array(
                    'class' => array('col-sm-10'),
                ),
            ),
            'col-inside' => array(
                'options' => array(
                    'class' => array('col-sm-offset-2', 'col-sm-10'),
                )
            )
        )
    );

    public function addCheckboxInlineClassIfGrouped($wrapper)
    {
        if ($this->strap()->getIsGrouped()) {
            $wrapper->setAttributes($this->addClasses($this->getDefault('label.grouped.checkbox.options.class'), $wrapper->getAttributes()));
        }
        return $wrapper;
    }

    public function addRadioInlineClassIfGrouped($wrapper)
    {
        if ($this->strap()->getIsGrouped()) {
            $wrapper->setAttributes($this->addClasses($this->getDefault('label.grouped.radio.options.class'), $wrapper->getAttributes()));
        }
        return $wrapper;
    }

    public function addErrorWrapper($wrapper)
    {
        if ($wrapper && $wrapper->getValue()) {
            $wrapper->setTag('span');
            $wrapper->setAttributes($this->addClasses($this->getDefault('error.options.class'), $wrapper->getAttributes()));
        }
        return $wrapper;
    }

    public function removeExtraValuesHidden($wrapper)
    {
        $values = $wrapper->getValue();
        $newvalue = array();
        foreach ($values as $value) {
            if ($value instanceof Input) {
                $newvalue[] = $value;
            }
        }
        if ($newvalue) {
            $wrapper->setValue($newvalue);
        }
        return $wrapper;
    }

    public function stripLabel($wrapper)
    {
        $labelparent = $this->recurseWrappersByType($wrapper, 'Label');
        if ($labelparent) {
            $newvalues = array();
            $values = $labelparent->getValue();
            foreach ($values as $value) {
                if (!$value instanceof Label) {
                    $newvalues[] = $value;
                }
            }
            if (!$newvalues) {
                $newvalues[] = '&nbsp;';
            }
            $labelparent->setValue($newvalues);
        }
        return $wrapper;
    }

    public function stripWrappers($wrapper)
    {
        $result = $this->recurseWrappersByType($wrapper, 'Input');
        if ($result) {
            return $result;
        }
        return $wrapper;
    }

    public function addColOneClasses($wrapper)
    {
        $labelparent = $this->recurseWrappersByType($wrapper, 'Label');
        if ($labelparent) {
            if ($labelparent->getTag() == '') {
                foreach ($labelparent->getValue() as $w) {
                    if ($w instanceof Label) {
                        $w->setAttributes($this->addClasses($this->getDefault('horizontal.label.options.class'), $wrapper->getAttributes()));
                    }
                }
            } else {
                $labelparent->setAttributes($this->addClasses($this->getDefault('horizontal.label.options.class'), $wrapper->getAttributes()));
            }
        } else {
            if (!$wrapper instanceof Label) {
                $wrapper->setTag('div');
            }
            $wrapper->setAttributes($this->addClasses($this->getDefault('horizontal.col-1.options.class'), $wrapper->getAttributes()));
        }
        return $wrapper;
    }

    public function addColTwoClasses($wrapper)
    {
        $wrapper->setTag('div');
        $wrapper->setAttributes($this->addClasses($this->getDefault('horizontal.col-2.options.class'), $wrapper->getAttributes()));
        return $wrapper;
    }

    public function addOffsetWrapper($wrapper)
    {
        $offset = new Wrapper('div', $this->getDefault('horizontal.col-inside.options'));
        $offset->setValue($wrapper);
        return $offset;
    }

    public function addCheckboxWrapperInsideFormGroupHorizontal($wrapper)
    {
        $w = new Wrapper('div', $this->getDefault('wrapper.checkbox.options.class'));
        $content = $wrapper->getValue();
        $w->setValue($content);
        $wrapper->setValue($w);
        return $wrapper;
    }

    public function addRadioWrapperInsideFormGroupHorizontal($wrapper)
    {
        $w = new Wrapper('div', $this->getDefault('wrapper.radio.options.class'));
        $content = $wrapper->getValue();
        $w->setValue($content);
        $wrapper->setValue($w);
        return $wrapper;
    }

    public function addFormControl($wrapper)
    {
        $wrapper->setAttributes($this->addClasses($this->getDefault('field.options.class'), $wrapper->getAttributes()));
        return $wrapper;
    }

    public function removeFormControl($wrapper)
    {
        $wrapper->setAttributes($this->removeClasses($this->getDefault('field.options.class'), $wrapper->getAttributes()));
        return $wrapper;
    }

    public function addFormGroup($wrapper)
    {
        if ($this->strap()->getWrap()) {
            $wrapper->setTag('div');
            $wrapper->setAttributes($this->addClasses($this->getDefault('outer.options.class'), $wrapper->getAttributes()));
        }
        return $wrapper;
    }

    public function removeFormGroup($wrapper)
    {
        $wrapper->setAttributes($this->removeClasses(array('form-group'), $wrapper->getAttributes()));
        return $wrapper;
    }

    public function addCheckboxClass($wrapper)
    {
        if ($this->strap()->getWrap()) {
            $wrapper->setTag('div');
            $wrapper->setAttributes($this->addClasses($this->getDefault('wrapper.checkbox.options.class'), $wrapper->getAttributes()));
        }
        return $wrapper;
    }

    public function addRadioClass($wrapper)
    {
        if ($this->strap()->getWrap()) {
            $wrapper->setTag('div');
            $wrapper->setAttributes($this->addClasses($this->getDefault('wrapper.radio.options.class'), $wrapper->getAttributes()));
        }
        return $wrapper;
    }

    public function checkButtonClasses($wrapper)
    {
        if ($this->strap()->getButtonify() !== false) {
            return $this->addButtonClasses($wrapper);
        }
        return $wrapper;
    }

    public function addButtonClasses($wrapper)
    {
        $classes = $this->getDefault('button.options.class');
        if ($this->strap()->getButtonify() !== false && $this->strap()->getButtonify() !== true) {
            $classes[] = $this->strap()->getButtonify();
        } else {
            $classes[] = \Smorken\Strap\Builders\Bootstrap::BTN_DEFAULT;
        }
        $wrapper->setAttributes($this->addClasses($classes, $wrapper->getAttributes()));
        return $wrapper;
    }

    public function wrapCheckboxIfHorizontal($wrapper)
    {
        if ($this->strap()->getType() == 'horizontal') {
            return $this->wrapCheckbox($wrapper);
        }
        return $wrapper;
    }

    public function wrapRadioIfHorizontal($wrapper)
    {
        if ($this->strap()->getType() == 'horizontal') {
            return $this->wrapRadio($wrapper);
        }
        return $wrapper;
    }

    public function wrapCheckbox($wrapper)
    {
        if (!$this->strap()->getIsGrouped()) {
            $new = new Wrapper('div', array('class' => array('checkbox')));
            $new->setValue($wrapper);
            return $new;
        }
        return $wrapper;
    }

    public function wrapRadio($wrapper)
    {
        if (!$this->strap()->getIsGrouped()) {
            $new = new Wrapper('div', array('class' => array('radio')));
            $new->setValue($wrapper);
            return $new;
        }
        return $wrapper;
    }

    public function addDefaultHorizontalFieldsWrapperClasses($wrapper)
    {
        if (!$this->strap()->getInside()) {

        }
    }

}