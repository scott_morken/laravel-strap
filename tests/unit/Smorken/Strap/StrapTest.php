<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/17/14
 * Time: 9:39 AM
 */

namespace unit\Smorken\Strap;

use Mockery as m;
use Smorken\Strap\FormBuilder;
use Illuminate\Html\HtmlBuilder;
use Illuminate\Support\Facades\App;

class StrapTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \Smorken\Strap\Strap
     */
    protected $sut;

    public function setUp()
    {
        $html = new HtmlBuilder();
        $urlmock = m::mock('Illuminate\Routing\UrlGenerator');
        $form = new FormBuilder($html, $urlmock, 'xyz');

        $this->sut = new \Smorken\Strap\Strap($form);
    }

    public function tearDown()
    {
        m::close();
    }

    public function testChainable()
    {
        App::shouldReceive('make')
            ->with('session')
            ->andReturn(false);
        $obj = $this->sut->text('name')->label('foo')->value(1)->options(array())->checked();
        $this->assertInstanceOf('Smorken\Strap\Strap', $obj);
    }

    public function testNameIsSetFrom__call()
    {
        App::shouldReceive('make')
            ->with('session')
            ->andReturn(false);
        $this->sut->text('name');
        $this->assertEquals('name', $this->sut->getName());
    }

    public function testCreateText()
    {
        App::shouldReceive('make')
            ->with('session')
            ->andReturn(false);
        $r = $this->sut->text('name');
        $expected = '<label for="name">Name</label> <input name="name" type="text">';
        $this->assertStringStartsWith($expected, (string) $r);
    }

    public function testCreateTextOverrideBuilder()
    {
        App::shouldReceive('make')
            ->with('session')
            ->andReturn(false);
        $r = $this->sut->text('name')->builder('bootstrap');
        $expected = '<div class="form-group"><label for="name">Name</label> <input class="form-control" name="name" type="text">  </div>';
        $this->assertStringStartsWith($expected, (string) $r);
    }

    public function testCreateHiddenOverrideBuilderStillSimple()
    {
        App::shouldReceive('make')
            ->with('session')
            ->andReturn(false);
        $r = $this->sut->hidden('name')->builder('bootstrap');
        $expected = '<input class="form-control" name="name" type="hidden">';
        $this->assertStringStartsWith($expected, (string) $r);
    }

    public function testSessionAddsError()
    {
        $messages = m::mock('StdClass');
        $messages->shouldReceive('first')
            ->once()
            ->with('name')
            ->andReturn('foo');
        $session = m::mock('StdClass');
        $session->shouldReceive('has')
            ->with('errors')
            ->once()
            ->andReturn(true);
        $session->shouldReceive('get')
            ->with('errors')
            ->once()
            ->andReturn($messages);
        $html = new HtmlBuilder();
        $urlmock = m::mock('Illuminate\Routing\UrlGenerator');
        $form = new FormBuilder($html, $urlmock, 'xyz');
        App::clearResolvedInstances();
        App::shouldReceive('make')
            ->with('session')
            ->once()
            ->andReturn($session);
        $sut = new \Smorken\Strap\Strap($form);
        $expected = '<div class="form-group"><label for="name">Name</label> <input class="form-control" name="name" type="text">  <span class="text-danger">foo</span></div>';
        $r = $sut->text('name')->builder('bootstrap');
        $this->assertEquals($expected, (string) $r);
    }
} 