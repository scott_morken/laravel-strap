<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/17/14
 * Time: 9:39 AM
 */

namespace unit\Smorken\Strap\Builders;

use Mockery as m;
use Illuminate\Html\HtmlBuilder;
use Smorken\Strap\FormBuilder;
use Smorken\Strap\Strap;
use Illuminate\Support\Facades\App;

class BaseTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \Smorken\Strap\Builders\Base
     */
    protected $sut;

    public function setUp()
    {
        $html = new HtmlBuilder();
        $urlmock = m::mock('Illuminate\Routing\UrlGenerator');
        $form = new FormBuilder($html, $urlmock, 'xyz');
        $this->sut = new \Smorken\Strap\Builders\Base();
        $this->sut->setFormBuilder($form);
    }

    public function tearDown()
    {
        m::close();
    }

    public function testbuildField()
    {
        $s = $this->createStrap();
        $this->sut->setStrap($s->text('test'));

        $this->sut->buildField();
        $fields = $this->sut->getField();
        $this->assertEquals('', $fields->getTag());
    }

    public function testBuildFieldWithValue()
    {
        $s = $this->createStrap()->text('test')->value('value');
        $this->sut->setStrap($s);
        $this->sut->buildField();
        $f = $this->sut->getField();
        $expected = '<input name="test" type="text" value="value">';
        $this->assertEquals($expected, (string) $f);
    }

    public function testBuildNumberField()
    {
        $s = $this->createStrap()->number('test');
        $this->sut->setStrap($s);
        $this->sut->buildField();
        $f = $this->sut->getField();
        $expected = '<input name="test" type="number">';
        $this->assertEquals($expected, (string) $f);
    }

    public function testBuildNumberFieldWithMin()
    {
        $s = $this->createStrap()->number('test', 1);
        $this->sut->setStrap($s);
        $this->sut->buildField();
        $f = $this->sut->getField();
        $expected = '<input min="1" name="test" type="number">';
        $this->assertEquals($expected, (string) $f);
    }

    public function testBuildNumberFieldWithMax()
    {
        $s = $this->createStrap()->number('test', null, 3);
        $this->sut->setStrap($s);
        $this->sut->buildField();
        $f = $this->sut->getField();
        $expected = '<input max="3" name="test" type="number">';
        $this->assertEquals($expected, (string) $f);
    }


    public function testBuildLabel()
    {
        $this->sut->setStrap($this->createStrap()->text('name')->label('foo'));
        $this->sut->buildLabel();
        $label = $this->sut->getLabel();
        $expected = '<label for="name">foo</label>';
        $this->assertEquals($expected, (string) $label);
    }

    public function testBuildLabelSkipBuild()
    {
        $strap = $this->createStrap();
        $s = $strap->label(false);
        $this->sut->setStrap($s);
        $this->sut->buildLabel();
        $label = $this->sut->getLabel();
        $expected = '';
        $this->assertEquals($expected, (string) $label);
    }

    public function testbuildFieldCheckbox()
    {
        $strap = $this->createStrap();
        $s = $strap->checkbox('name');
        $this->sut->setStrap($s);
        $this->sut->buildField();
        $chk = $this->sut->getField();
        $expected = '<input name="name" type="checkbox" value="1">';
        $this->assertEquals($expected, (string) $chk);
    }

    public function testbuildFieldCheckboxChecked()
    {
        $strap = $this->createStrap();
        $s = $strap->checkbox('name')->checked();
        $this->sut->setStrap($s);
        $this->sut->buildField();
        $chk = $this->sut->getField();
        $expected = '<input checked="checked" name="name" type="checkbox" value="1">';
        $this->assertEquals($expected, (string) $chk);
    }

    public function testbuildFieldSelectEmpty()
    {
        $strap = $this->createStrap();
        $s = $strap->select('name');
        $this->sut->setStrap($s);
        $this->sut->buildField();
        $r = $this->sut->getField();
        $expected = '<select name="name"></select>';
        $this->assertEquals($expected, (string) $r);
    }

    public function testbuildFieldSelectPopulated()
    {
        $strap = $this->createStrap();
        $s = $strap->select('name')->data(array(1 => 'Value 1', 2 => 'Value 2'));
        $this->sut->setStrap($s);
        $this->sut->buildField();
        $r = $this->sut->getField();
        $expected = '<select name="name"><option value="1">Value 1</option><option value="2">Value 2</option></select>';
        $this->assertEquals($expected, (string) $r);
    }

    public function testBuildFieldSelectPopulatedWithSelection()
    {
        $strap = $this->createStrap();
        $s = $strap->select('name')->data(array(1 => 'Value 1', 2 => 'Value 2'))->value(2);
        $this->sut->setStrap($s);
        $this->sut->buildField();
        $r = $this->sut->getField();
        $expected = '<select name="name"><option value="1">Value 1</option><option value="2" selected="selected">Value 2</option></select>';
        $this->assertEquals($expected, (string) $r);
    }

    public function testHelp()
    {
        $this->sut->setStrap($this->createStrap()->help('HELP'));
        $this->sut->buildHelp();
        $expected = 'HELP';
        $this->assertEquals($expected, (string) $this->sut->getHelp());
    }

    public function testBuildLabelEmptyLabel()
    {
        $strap = $this->createStrap();
        $s = $strap->text('name')->label('');
        $this->sut->setStrap($s);
        $this->sut->buildLabel();
        $label = $this->sut->getLabel();
        $expected = '<label for="name">Name</label>';
        $this->assertEquals($expected, (string) $label);
    }

    public function testBuildError()
    {
        $strap = $this->createStrap();
        $this->sut->setStrap($strap->error('baz'));
        $this->sut->buildError();
        $error = $this->sut->getError();
        $expected = 'baz';
        $this->assertEquals($expected, (string) $error);
    }

    public function testBuildCheckboxInsideLabelOnRight()
    {
        $strap = $this->createStrap();
        $s = $strap->checkbox('name')->label('foo')->inside();
        $this->sut->setStrap($s);
        $this->sut->buildField();
        $this->sut->buildLabel();
        $o = $this->sut->getLabel();
        $expected = '<label for="name"><input name="name" type="checkbox" value="1"> foo</label>';
        $this->assertEquals($expected, (string) $o);
    }

    public function testBuildCheckboxInsideLabelOnLeft()
    {
        $strap = $this->createStrap();
        $s = $strap->checkbox('name')->inside()->left()->label('foo');
        $this->sut->setStrap($s);
        $this->sut->buildField();
        $this->sut->buildLabel();
        $o = $this->sut->getLabel();
        $expected = '<label for="name">foo <input name="name" type="checkbox" value="1"></label>';
        $this->assertEquals($expected, (string) $o);
    }

    public function testWrapIndividualItemsInside()
    {
        $this->sut->setStrap($this->createStrap()->text('test')->inside());
        $this->sut->buildField();
        $this->sut->buildLabel();
        $this->sut->buildHelp();
        $this->sut->buildError();
        $wraps = $this->sut->getWrapOptions();
        $items = $this->sut->wrapIndividualItems($wraps);
        $this->assertEquals($items['label'], $this->sut->getLabel());
        $this->assertEquals(3, count($items));
    }

    public function testGroupItems()
    {
        $this->sut->setStrap($this->createStrap()->text('test'));
        $this->sut->buildField();
        $this->sut->buildLabel();
        $this->sut->buildHelp();
        $this->sut->buildError();
        $wraps = $this->sut->getWrapOptions();
        $items = $this->sut->wrapIndividualItems($wraps);
        $grouped = $this->sut->groupItems($items);
        $this->assertArrayHasKey('col', $grouped);
        $this->assertEquals(4, count($grouped['col']));
    }

    public function testWrapGroupedItems()
    {
        $this->sut->setStrap($this->createStrap()->text('test'));
        $this->sut->buildField();
        $this->sut->buildLabel();
        $this->sut->buildHelp();
        $this->sut->buildError();
        $wraps = $this->sut->getWrapOptions();
        $items = $this->sut->wrapIndividualItems($wraps);
        $grouped = $this->sut->groupItems($items);
        $wrapped = $this->sut->wrapGroupedItems($grouped);
        $this->assertEquals($grouped['col'], $wrapped['col']->getValue());
    }

    public function testOuterWrapper()
    {
        $this->sut->setStrap($this->createStrap()->text('name')->label('foo')->help('HELP')->error('baz'));
        $this->sut->buildField();
        $this->sut->buildLabel();
        $this->sut->buildHelp();
        $this->sut->buildError();
        $wraps = $this->sut->getWrapOptions();
        $items = $this->sut->wrapIndividualItems($wraps);
        $grouped = $this->sut->groupItems($items);
        $wrapped = $this->sut->wrapGroupedItems($grouped);
        $outer = $this->sut->createOuterWrapper($wrapped);
        $expected = '<label for="name">foo</label> <input name="name" type="text"> HELP baz';
        $this->assertEquals($expected, (string) $outer);
    }

    public function testOuterWrapperIsBuild()
    {
        $this->sut->setStrap($this->createStrap()->text('name'));
        $this->sut->buildField();
        $this->sut->buildLabel();
        $this->sut->buildHelp();
        $this->sut->buildError();
        $wraps = $this->sut->getWrapOptions();
        $items = $this->sut->wrapIndividualItems($wraps);
        $grouped = $this->sut->groupItems($items);
        $wrapped = $this->sut->wrapGroupedItems($grouped);
        $outer = $this->sut->createOuterWrapper($wrapped);
        $this->assertEquals((string) $outer, (string) $this->sut->build());
    }

    public function testLink()
    {
        $strap = $this->createStrap()->link('www.google.com');
        $this->sut->setStrap($strap);
        $this->sut->buildField();
        $w = $this->sut->getField();
        $expected = '<a href="www.google.com" title="Link to www.google.com">www.google.com</a>';
        $this->assertEquals($expected, (string) $w);
    }

    public function testLinkWithTitle()
    {
        $strap = $this->createStrap()->link('www.google.com', 'Google');
        $this->sut->setStrap($strap);
        $this->sut->buildField();
        $w = $this->sut->getField();
        $expected = '<a href="www.google.com" title="Link to Google">Google</a>';
        $this->assertEquals($expected, (string) $w);
    }

    public function testIdOverridesNameForLabel()
    {
        $this->sut->setStrap($this->createStrap()->text('name')->label('Label')->options(array('id' => 'id')));
        $this->sut->buildField();
        $this->sut->buildLabel();
        $l = $this->sut->getLabel();
        $expected = '<label for="id">Label</label>';
        $this->assertEquals($expected, (string)$l);
    }


    protected function createStrap()
    {
        App::shouldReceive('make')
            ->with('session')
            ->andReturn(false);
        $html = new HtmlBuilder();
        $urlmock = m::mock('Illuminate\Routing\UrlGenerator');
        $form = new FormBuilder($html, $urlmock, 'xyz');
        $strap = new Strap($form);
        return $strap;
    }
} 