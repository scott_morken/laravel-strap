<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/18/14
 * Time: 1:12 PM
 */

namespace unit\Smorken\Strap\Builders\Modifiers;

use Smorken\Strap\Builders\Modifiers\AbstractMod;

class ModStub extends AbstractMod {

    protected $handlers = array(
        'foo' => array(
            'method' => 'dummy',
            'bar' => array(
                'baz' => array(
                    'method' => 'dummy',
                ),
            ),
            'bazzle' => array(
                'method' => 'dummy2',
            )
        ),
        'biz' => array(
            'method' => 'dummy3',
            'boo' => array(
                'method' => 'notexists',
            ),
        ),
    );

    public function dummy($arg) { return true; }
    public function dummy2($arg) { return 'foo'; }
    public function dummy3($arg) { return $arg; }
}

class ModStub2 extends ModStub {
    protected $childHandlers = array(
        'foo' => array(
            'method' => 'dummy2',
        ),
        'biz' => array(
            'boo' => array(
                'method' => 'notexists2',
            ),
        ),
    );
}

class ModTest extends \PHPUnit_Framework_TestCase {

    public function testModMethodDummy()
    {
        $sut = new ModStub(null);
        $this->assertTrue($sut->mod('foo', 0, 1));
    }

    public function testModMethodDummyDeep()
    {
        $sut = new ModStub(null);
        $this->assertTrue($sut->mod('foo.bar.baz', false, 1));
    }

    /**
     * @expectedException Smorken\Strap\Builders\Modifiers\ModException
     */
    public function testModThrowsModExceptionWithoutArgs()
    {
        $sut = new ModStub(null);
        $this->assertTrue($sut->mod('foo', false));
    }

    /**
     * @expectedException Smorken\Strap\Builders\Modifiers\ModException
     */
    public function testModThrowsModExceptionInvalidMethod()
    {
        $sut = new ModStub(null);
        $this->assertTrue($sut->mod('foo.biz.boo', false));
    }

    public function testModChildHandlersAddToMethods()
    {
        $sut = new ModStub2(null);
        $expected = array(
            'foo' => array(
                'method' => array('dummy', 'dummy2'),
                'bar' => array(
                    'baz' => array(
                        'method' => 'dummy',
                    ),
                ),
                'bazzle' => array(
                    'method' => 'dummy2',
                )
            ),
            'biz' => array(
                'method' => 'dummy3',
                'boo' => array(
                    'method' => array('notexists', 'notexists2'),
                ),
            ),
        );
        $this->assertEquals($expected, $sut->getHandlers());
    }

}