<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/17/14
 * Time: 9:25 AM
 */

namespace Smorken\Strap\Parts;


class Wrapper
{
    protected $value;

    protected $tag = 'div';

    protected $attributes = null;

    public function __construct($tag = 'div', $attributes = null)
    {
        if (!$tag) {
            $this->setEmpty();
        }
        else {
            $this->setTag($tag);
            $this->setAttributes($attributes);
        }
    }

    public function getTag()
    {
        return $this->tag;
    }

    public function setTag($tag = 'div')
    {
        if (ends_with($tag, '>')) {
            $tag = substr($tag, 0, strlen($tag) - 1);
        }
        if (starts_with($tag, '<')) {
            $tag = substr($tag, 1);
        }
        $this->tag = $tag;
        return $this;
    }

    public function setEmpty()
    {
        $this->tag = '';
        $this->attributes = null;
    }

    public function addValue($value, $append = true)
    {
        if (!is_array($this->value)) {
            $this->value = array($this->value);
        }
        if ($append) {
            $this->value[] = $value;
        }
        else {
            array_unshift($this->value, $value);
        }
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        $v = $this->value;
        if (!is_array($v)) {
            $v = array($v);
        }
        return $v;
    }

    /**
     * @param null $attributes
     * @return $this
     */
    public function setAttributes($attributes = null)
    {
        $this->attributes = $attributes;
        return $this;
    }

    public function addAttribute($value, $key = null, $append = false)
    {
        if ($key !== null) {
            $prev_value = isset($this->attributes[$key]) ? $this->attributes[$key] : '';
            if (is_array($prev_value) && $append) {
                $this->attributes[$key][] = $value;
            }
            else if (!is_array($prev_value) && $append) {
                $this->attributes[$key] = ($append ? $prev_value . ' ' : '') . $value;
            }
            else if (!$append) {
                $this->attributes[$key] = $value;
            }
        }
        else {
            $this->attributes[] = $value;
        }
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Build an HTML attribute string from an array.
     *
     * @param  array  $attributes
     * @return string
     */
    public function attributesToString($attributes)
    {
        $html = array();

        // For numeric keys we will assume that the key and the value are the same
        // as this will convert HTML attributes such as "required" to a correct
        // form like required="required" instead of using incorrect numerics.
        foreach ((array) $attributes as $key => $value)
        {
            $element = $this->attributeElement($key, $value);

            if ( ! is_null($element)) $html[] = $element;
        }

        return count($html) > 0 ? ' '.implode(' ', $html) : '';
    }

    /**
     * Build a single attribute element.
     *
     * @param  string  $key
     * @param  string  $value
     * @return string
     */
    protected function attributeElement($key, $value)
    {
        if (is_array($value)) {
            $value = implode(' ', $value);
        }
        $value = trim($value);
        if (is_numeric($key)) $key = $value;

        if ( ! is_null($value)) return $key.'="'.e($value).'"';
    }


    public function wrap($html = null, $attributes = null)
    {
        if ($html !== null) {
            $this->setValue($html);
        }
        if ($attributes !== null) {
            $this->setAttributes($attributes);
        }

        return $this->start() . $this->end();
    }

    public function getText()
    {
        return $this->convertToString($this->getValue());
    }

    protected function convertToString($html)
    {
        if (!is_array($html)) {
            $html = array($html);
        }
        array_walk($html, function(&$v, $k) {
            $v = (string) $v;
        });
        return implode(' ', $html);
    }

    public function start()
    {
        if (!$this->tag) {
            return $this->getText();
        }

        $wrap = '';
        $out = $wrap . '<' . $this->tag . $this->attributesToString($this->attributes) . '>';
        $out .= $this->getText();
        return $out;
    }

    public function end()
    {
        $wrap = '';
        $out = '';
        if ($this->tag) {
            $out .= '</' . $this->tag . '>';
        }
        $out .= $wrap;
        return $out;
    }

    public function __toString()
    {
        return $this->wrap();
    }
} 