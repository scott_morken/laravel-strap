<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/16/14
 * Time: 1:55 PM
 */

namespace Smorken\Strap\Facades;

use Illuminate\Support\Facades\Facade;

class Strap extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'strap'; }

} 