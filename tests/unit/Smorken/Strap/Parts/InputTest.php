<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/21/14
 * Time: 10:21 AM
 */

namespace unit\Smorken\Strap\Parts;

use Mockery as m;
use Smorken\Strap\Parts\Input;
use Smorken\Strap\FormBuilder;
use Illuminate\Html\HtmlBuilder;

class InputTest extends \PHPUnit_Framework_TestCase {

    protected $form;

    public function setUp()
    {
        $html = new HtmlBuilder();
        $urlmock = m::mock('Illuminate\Routing\UrlGenerator');
        $this->form = new FormBuilder($html, $urlmock, 'xyz');
    }

    public function tearDown()
    {
        m::close();
    }

    public function testText()
    {
        $w = new Input($this->form, 'text', array('Test'));
        $expected = '<input name="Test" type="text">';
        $this->assertEquals($expected, $w->wrap());
    }

    public function testAddAttribute()
    {
        $w = new Input($this->form, 'text', array('Test'));
        $w->addAttribute('someclass', 'class');
        $expected = '<input class="someclass" name="Test" type="text">';
        $this->assertEquals($expected, $w->wrap());
    }

    public function testAddAttributeCanAppend()
    {
        $w = new Input($this->form, 'text', array('Test', null, array('class' => 'class1')));
        $w->addAttribute('someclass', 'class', true);
        $expected = '<input class="class1 someclass" name="Test" type="text">';
        $this->assertEquals($expected, $w->wrap());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testThrowsInvalidArgumentException()
    {
        $w = new Input($this->form, 'foo', array());
    }
} 